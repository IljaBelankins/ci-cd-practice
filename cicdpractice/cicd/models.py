from django.db import models


class CalculationRequest(models.Model):
    int1 = models.IntegerField()
    int2 = models.IntegerField()
    operand = models.CharField(max_length=1)
    result = models.IntegerField()
    created_at = models.DateField(auto_now_add=True)
