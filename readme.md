# Build in Docker
To make a build in docker and then run it, run the following in the Dockerfile directory:

'''
docker build -t {insert tag here} . 
docker run -p 8000:80 {use tag here}
'''

# Cancel Docker ports
If containers break, run the following commands:

'''
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
'''

# Steps for running without docker compose
First, build the postgres image and give it a name. Run this in the Dockerfile dir:
'''
docker build . -t django_app
'''

Next, create a network:
'''
docker network create mynet
'''

Next, create a  postgres image (in detached mode). Remember to add it to the net and to give give it a name:

'''
docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password --net mynet --name Database postgres
'''

Next, run a migration against the database (remember to implement the name you used and to use the net)

'''
docker run -e DATABASE_HOST=Database --net mynet django_app python3 manage.py migrate
'''
