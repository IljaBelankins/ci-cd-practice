import requests
import json


def test_api_adds ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    response.raise_for_status()
    response_data = response.json()
    assert "result" in response_data
    assert response_data["result"] == 3

def test_api_subtract ():
    data = {
        "int1": 7,
        "int2": 2,
        "operand": "-"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    response.raise_for_status()
    response_data = response.json()
    assert "result" in response_data
    assert response_data["result"] == 5

def test_api_incorrect_type ():
    data = {
        "int1": "oops",
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_incorrect_operand ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "*"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_adds_negative ():
    data = {
        "int1": -1,
        "int2": -2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    response.raise_for_status()
    response_data = response.json()
    assert "result" in response_data
    assert response_data["result"] == -3

def test_api_subtract_negative ():
    data = {
        "int1": -1,
        "int2": -2,
        "operand": "-"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    response.raise_for_status()
    response_data = response.json()
    assert "result" in response_data
    assert response_data["result"] == 1

def test_api_decimals ():
    data = {
        "int1": 1.05,
        "int2": 2.05,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_wrong_method_get ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.get("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 405

def test_api_wrong_method_put ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.put("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 405

def test_api_wrong_method_delete ():
    response = requests.delete("https://ci-cd-practice-staging.herokuapp.com/cicd/api")
    assert response.status_code == 405

def test_api_missing_operand ():
    data = {
        "int1": 1,
        "int2": 2,
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_large_number ():
    data = {
        "int1": 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111,
        "int2": 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_large_negative_number ():
    data = {
        "int1": -1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111,
        "int2": -1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_missing_integer ():
    data = {
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 400

def test_api_missing_headers ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "+"
    }
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data))
    assert response.status_code == 415

def test_api_wrong_format ():
    headers = {"Content-Type": "application/json"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", "this is not json", headers=headers)
    assert response.status_code == 400

def test_api_wrong_headers ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/html"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/api", json.dumps(data), headers=headers)
    assert response.status_code == 415

def test_api_wrong_endpoint ():
    data = {
        "int1": 1,
        "int2": 2,
        "operand": "+"
    }
    headers = {"Content-Type": "application/html"}
    response = requests.post("https://ci-cd-practice-staging.herokuapp.com/cicd/apiiiii", json.dumps(data), headers=headers)
    assert response.status_code == 404