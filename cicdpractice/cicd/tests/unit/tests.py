from cicd.utils import calculate
from django.test import TestCase
import pytest


class TestingCalculation(TestCase):
    def test_addition(self):
        result = calculate(2, 5, "+")
        assert result == 7

    def test_subtraction(self):
        result = calculate(10, 4, "-")
        assert result == 6

    def test_addition_string(self):
        with pytest.raises(TypeError):
            calculate(2, "5", "+")

    def test_subtraction_string(self):
        with pytest.raises(TypeError):
            result = calculate(10, "4", "-")
            assert result == 6

    def test_subtraction_wrong_operand(self):
        with pytest.raises(ValueError):
            calculate(10, 5, "/")


'''
class TestingEndPointAddition(TestCase):
    def setup (self):
        int1 = 6
        int2 = 4
        operand = "+"
        url = reverse ("math")

class TestingEndPointSubtraction(TestCase):
    def setup (self):
        int1 = 6
        int2 = 4
        operand = "-"

class TestingEndPointAdditionString(TestCase):
    def setup (self):
        int1 = 6
        int2 = "4"
        operand = "+"

class TestingEndPointSubtractionString(TestCase):
    def setup (self):
        int1 = 6
        int2 = "4"
        operand = "-"

class TestingEndPointWrongOperand(TestCase):
    def setup (self):
        int1 = 6
        int2 = "4"
        operand = "*"

class TestingEndPointMissingOperand(TestCase):
    def setup (self):
        int1 = 6
        int2 = 4
        operand = None

class TestingEndPointMissingInt1(TestCase):
    def setup (self):
        int1 = None
        int2 = 4
        operand = "+"

class TestingEndPointMissingInt2(TestCase):
    def setup (self):
        int1 = 6
        int2 = None
        operand = "+"

class TestingEndPointMissingInts(TestCase):
    def setup (self):
        int1 = None
        int2 = None
        operand = "+"

class TestingEndPointMissingAll(TestCase):
    def setup (self):
        int1 = None
        int2 = None
        operand = None
'''
