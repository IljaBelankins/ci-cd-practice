from django.test import TestCase, Client
from django.urls import reverse
import json


class WishlistViewTestCase(TestCase):
    def setup(self):
        pass

    def test_view_addition(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": 4,
            "operand": "+"}),
            content_type="application/json")
        assert response.status_code == 200
        result = response.json()
        assert result["result"] == 10

    def test_view_subtraction(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": 4,
            "operand": "-"}),
            content_type="application/json")
        assert response.status_code == 200
        result = response.json()
        assert result["result"] == 2

    def test_view_invalid_operand(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": 4,
            "operand": "*"}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_wrong_operand(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": 4,
            "operand": 1}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_wrong_int1_type(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": "6",
            "int2": 4,
            "operand": "+"}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_wrong_int2_type(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": "4",
            "operand": "+"}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_missing_int1(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": None,
            "int2": "4",
            "operand": "+"}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_missing_int2(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": None,
            "operand": "+"}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_missing_operand(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 6,
            "int2": 4,
            "operand": None}),
            content_type="application/json")
        assert response.status_code == 400

    def test_view_math_incorrect_method(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.get(url)
        assert response.status_code == 405

    def test_view_large(self):
        self.setup()
        client = Client()
        url = reverse("api")
        response = client.post(url, json.dumps({
            "int1": 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111,
            "int2": 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111,
            "operand": "+"}),
            content_type="application/json")
        assert response.status_code == 400
