from locust import HttpUser, between, task
import json

class WebsiteUser(HttpUser):
    wait_time = between(5, 15)
    
    @task(1)
    def add(self):
        headers = {
            "Content-Type": "application/json"
        }
        self.client.post("/api", 
        json.dumps({
            "int1": 1,
            "int2": 5,
            "operand": "+"
        }),
        headers=headers)

    @task(2)
    def add_2(self):
        headers = {
            "Content-Type": "application/json"
        }
        self.client.post("/api", 
        json.dumps({
            "int1": 5,
            "int2": 6,
            "operand": "+"
        }),
        headers=headers)

    @task(3)
    def subtract(self):
        headers = {
            "Content-Type": "application/json"
        }
        self.client.post("/api", 
        json.dumps({
            "int1": 25,
            "int2": 1,
            "operand": "-"
        }),
        headers=headers)

    @task(4)
    def subtract_2(self):
        headers = {
            "Content-Type": "application/json"
        }
        self.client.post("/api", 
        json.dumps({
            "int1": 25,
            "int2": 7,
            "operand": "-"
        }),
        headers=headers)
        
