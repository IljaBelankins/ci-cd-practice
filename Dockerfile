from alpine:latest
add . /my-app
workdir /my-app
run apk update
run apk add postgresql-dev gcc python3-dev musl-dev
run apk add py3-pip
run apk add python3
run pip3 install psycopg2-binary
run pip3 install -r requirements.txt
workdir /my-app/cicdpractice
cmd ["python3", "manage.py", "runserver", "0.0.0.0:8000"]