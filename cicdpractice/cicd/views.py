from django.http import JsonResponse
from cicd.models import CalculationRequest
from rest_framework.request import Request
from rest_framework.decorators import parser_classes, api_view
from rest_framework.parsers import JSONParser
from rest_framework.exceptions import ParseError, MethodNotAllowed
from django.views.decorators.csrf import csrf_exempt
from cicd.utils import calculate


def index(request):
    return "index"


@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def math(request: Request):
    if request.method == 'POST':
        int1 = request.data.get("int1")
        int2 = request.data.get("int2")
        operand = request.data.get("operand")
        if int1 is None or int2 is None or operand is None:
            raise ParseError(
                detail="Math numbers and operand must be provided.")
        if not isinstance(int1, int)\
                or not isinstance(int2, int)\
                or not isinstance(operand, str):
            raise ParseError(
                detail="Math numbers and operand must be valid type.")
        if int1 <-2147483648 or int1 >2147483647 or int2 <-2147483648 or int2 >2147483647:
            raise ParseError(
                detail="Integers too small/big.")
        try:
            data = {"result": calculate(int1, int2, operand)}
        except ValueError:
            raise ParseError(detail="Error in calculate function.")
        calculation_request = CalculationRequest(
            int1=int1,
            int2=int2,
            operand=operand,
            result=data["result"])
        calculation_request.save()

        return JsonResponse(data, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)
