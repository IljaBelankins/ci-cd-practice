def calculate(int1_in: int, int2_in: int, operand_in: str) -> int:
    if operand_in == "+":
        result = int1_in + int2_in
        return result
    if operand_in == "-":
        result = int1_in - int2_in
        return result
    raise ValueError("Unsupported operand")
